# 今日笔记
## 二.路由篇
```
1.打开两个路由器，然后用铜直线连接；

```
![lmt](./img/31.png)
```
2.登录切换到全局模式，然后输入ho 命名；
```
![lmt](./img/32.png)
```
3.设置登录欢迎信息
```
![lmt](./img/33.png)
```
4.配置Telnet密码
```
![lmt](./img/34.png)
```

5.配置路由器接口：然后再全局模式里面配置用：interface GigabitEthernet 0/0/0设置接口类型，然后再用ip address配置IP地址；设置后要no shutdown；然后退出全局模式，用特权模式，检测接口状态：show ip interface brief
```
![lmt](./img/35.png)
```
6.检测连通性：用ping再加IP地址（在用户模式或者特权模式之下）
```
![lmt](./img/36.png)
```
7.telnet远程登陆到设备：用telnet加另一个设备的IP地址（一定要设置vty密码才行），然后输入另一个设备的密码就进入了另一个设备里面；若要挂起该设备。就：ctrl+shift+6，然后按住x
```
![lmt](./img/37.png)

# 我真是天才~(●'◡'●)